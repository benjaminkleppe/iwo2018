# README #

In this directory there are several files for the course Inleiding Wetenschappelijk Onderzoek.

Place the corresponding excel file in the same directory in order to run eerste-jaars-extracter.py

Enter the following in your terminal:
python3 eerste-jaars-extracter.py filename

The filename is the name of the excel file with the data of student enrolments. 
Make sure you enter the entire file name. In my case this is Eerste-jaar-WO-studenten.xlsx 

I provided the data set of the research. It contains all the university student enrolments of 2013-2017.

The program will return the number of students of the chosen field of studies of the corresponding years.