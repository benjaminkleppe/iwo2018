#!/usr/bin/python3
# Name: eerste-jaars-extracter.py
# This program returns the amount of first-year university students
# for a specific study direction in 2013, 2014, 2016 and 2017.
# Date: 20-03-2018
# Author: Benjamin Kleppe


import zipfile
import sys


#I found this function on StackOverload, it reads excel pages and returns a list.
#https://stackoverflow.com/questions/4371163/reading-xlsx-files-using-python
def excel_reader(fname):
    from xml.etree.ElementTree import iterparse
    z = zipfile.ZipFile(fname)
    strings = [el.text for e, el in iterparse(z.open('xl/sharedStrings.xml')) if el.tag.endswith('}t')]
    rows = []
    row = {}
    value = ''
    for e, el in iterparse(z.open('xl/worksheets/sheet1.xml')):
        if el.tag.endswith('}v'):
            value = el.text
        if el.tag.endswith('}c'):
            if el.attrib.get('t') == 's':
                value = strings[int(value)]
            letter = el.attrib['r']
            while letter[-1].isdigit():
                letter = letter[:-1]
            row[letter] = value
            value=''
        if el.tag.endswith('}row'):
            rows.append(row)
            row={}
    return rows

#This function removes excessive data from the excel sheet.
def remove_metadata(study_list):
    first_year_studies = []
    for lines in study_list:
        if lines['M'] == 'propedeuse bachelor' or lines['M'] == 'bachelor' and lines['L'] == 'voltijd onderwijs':
            first_year_studies.append(lines)
    for lines in first_year_studies:
        del lines['A']
        del lines['B']
        del lines['C']
        del lines['D']
        del lines['E']
        del lines['F']
        del lines['G']
        del lines['I']
        del lines['J']
        del lines['L']
        del lines['M']
        del lines['R']
        del lines['S']
    return first_year_studies

#In the main the programme looks for a study as input, then calculates the number of eerstejaars.
def main(argv):
    print("This program returns the amount of first-year university students")
    print("for a specific study direction in 2013, 2014, 2016 and 2017.")
    excel_lines = excel_reader(argv[1])
    clean_list = remove_metadata(excel_lines)
    thirteen = 0
    fourteen = 0
    sixteen = 0
    seventeen = 0
    studie_onderdeel = input("Enter the study direction (exactly the same as in the sheet): \n")
    for lines in clean_list:
        if lines['H'] == studie_onderdeel:
            thirteen += (int(lines['N']) + int(lines['O']))
            fourteen += (int(lines['P']) + int(lines['Q']))
            sixteen += (int(lines['T']) + int(lines['U']))
            seventeen += (int(lines['V']) + int(lines['W']))
    print('2013:',thirteen,'students\n2014:',fourteen,'students\n2016:',sixteen,
          'students\n2017:',seventeen,'students')


if __name__ == "__main__":
    main(sys.argv)
            
