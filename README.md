# README #

In this directory there are several files for the course Inleiding Wetenschappelijk Onderzoek.

In order to run de_extracter.txt, you need to download the file "RUG_wiki_page.txt"  aswell, 
and have them both in the same directory.
If this file does not work, please check whether the user, group and others all have the
permission to run this file.
Enter the following in your terminal, while in the directory with de_extracter.txt:
chmod +x de_extracter.txt